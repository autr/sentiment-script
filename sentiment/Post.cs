﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sentiment {
    public class Post {
        public int score { get; set; }
        public string title { get; set; }
        public string permalink { get; set; }
        public string topic { get; set; }
        public int simpleSentiment { get; set; }
        public double afinnSentiment { get; set; }
        public int afinnSentimentSimplified { get; set; }
        public int datumboxSentiment { get; set; }
        public int twitterSentiment { get; set; }
    }
}
