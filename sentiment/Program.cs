﻿using CsvHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace sentiment {
    class Program {
        static void Main(string[] args) {
            DatumboxAPI DatumboxAPI = new DatumboxAPI("e634a195cb47707a93071739a2219804");

            string[] delimiters = { " ", ", ", "...", ".", "'" };

            Dictionary<string, int> sentimentDict = new Dictionary<string, int> {
                {"positive",1},
                {"neutral",0},
                {"negative",-1}
            };

            List<Post> records = new List<Post>();
            using (var reader = new StreamReader("Showerthoughts_proc_twitter.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture)) {
                csv.Configuration.RegisterClassMap<PostMap>();
                records = csv.GetRecords<Post>().ToList();
            }

            double[] simpleMethodResults = new double[1000];

            var positive = new List<string>(File.ReadAllLines("bing-positive-words.txt"));
            var negative = new List<string>(File.ReadAllLines("bing-negative-words.txt"));

            var afinn = File.ReadLines("AFINN-111.txt").Select(line => line.Split("\t")).ToDictionary(line => line[0], line => line[1]);

            foreach (Post post in records) {
                double p = 0;
                double n = 0;
                int a = 0;
                string[] words = post.title.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                foreach (string word in words) {
                    string trimmedWord = word.Replace("-", "");
                    trimmedWord = trimmedWord.Replace("\\", "");
                    trimmedWord = trimmedWord.Replace("\"", "");
                    trimmedWord = trimmedWord.Replace(" ", "");
                    if (positive.Contains(trimmedWord.ToLower())) {
                        p++;
                    }
                    if (negative.Contains(trimmedWord.ToLower())) {
                        n++;
                    }
                    if (afinn.ContainsKey(trimmedWord.ToLower())) {
                        a += int.Parse(afinn[trimmedWord.ToLower()]);
                    }
                }
                post.simpleSentiment = (p / (p + n)) > 0.5 ? 1 : -1;
                post.afinnSentiment = (double)a / words.Count();
                if (post.afinnSentiment > 0) {
                    post.afinnSentimentSimplified = 1;
                } else if (post.afinnSentiment == 0) {
                    post.afinnSentimentSimplified = 0;
                } else if (post.afinnSentiment < 0) {
                    post.afinnSentimentSimplified = -1;
                }
                /*string topic = DatumboxAPI.TopicClassification(post.title);
                Sentiment sentiment = JsonConvert.DeserializeObject<Sentiment>(topic);
                Console.WriteLine(topic + " ---- " + sentiment.Output.Result);
                post.topic = sentiment.Output.Result;*/
                /*string datumboxSentiment = DatumboxAPI.SentimentAnalysis(post.title);
                Sentiment sentiment = JsonConvert.DeserializeObject<Sentiment>(datumboxSentiment);
                Console.WriteLine(sentiment.Output.Result);
                post.datumboxSentiment = sentimentDict[sentiment.Output.Result];*/
                /*string twitterSentiment = DatumboxAPI.TwitterSentimentAnalysis(post.title);
                Sentiment sentiment = JsonConvert.DeserializeObject<Sentiment>(twitterSentiment);
                Console.WriteLine(sentiment.Output.Result);
                post.twitterSentiment = sentimentDict[sentiment.Output.Result];*/
            }

            using (var writer = new StreamWriter("Showerthoughts_proc3.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture)) {
                csv.WriteRecords(records);
            }
        }
    }
}
