﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace sentiment {
    public sealed class PostMap : ClassMap<Post> {
        public PostMap() {
            AutoMap(CultureInfo.InvariantCulture);
            Map(m => m.simpleSentiment).Ignore();
            Map(m => m.afinnSentiment).Ignore();
            Map(m => m.afinnSentimentSimplified).Ignore();
        }
    }
}
